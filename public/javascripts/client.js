/*
  DESIGN PATTERNS QUIZ - WEB CLIENT
  
  EDWIN ANTONIO GONZÁLEZ URZUA 1167603
  GUILLERMO JUÁREZ DURÁN  1168346
*/

'use strict'

$(document).ready(function(){
    
    document.getElementById('audio').addEventListener('ended', function(){
      this.currentTime = 0;
      this.play();
    }, false);
    
    $(".mainMenu").click(function(){
       $.ajax({
        url: '/Quit',
        type: 'GET',
        error: connectionError,
        success: function(result) {
          $(".section").addClass('invisible')
          $("#index").removeClass('invisible')
          play('Grunty');
        }
      });
    });
    
    $("#up").click(function(){
      var questions= parseInt($("#questionsValue").text());
      if (questions < 10) {
        questions++;
        play('Change')
      }
      $("#questionsValue").html(questions);
    });
    
    $("#down").click(function(){
      var questions= parseInt($("#questionsValue").text());
      if (questions > 1) {
        questions--;
        play('Change')        
      }
      $("#questionsValue").html(questions);
    });
    
    $("#begin").click(function(){
      var data = {action: 'none', questions: parseInt($("#questionsValue").text())};
      game(data)
      play('Grunty');      
    });
    
    $(".answer").hover(function(){
       var dialog = $(this).attr('id');
       var length = dialog.length
       var id = parseInt(dialog.charAt(length -1))
       switch(id){
         case 1: play('Tooty')
                  break
         case 2: play('Mumbo')
                  break
         case 3: play('Cheato')
                  break
         case 4: play('Bottles')
                  break                  
       }
       $('#' + dialog).removeClass('not-hover')
    }, 
    function(){
      $('.answer').addClass('not-hover')
    });
    
    $(".answer").click(function(){
      var dialog = "#" + $(this).attr('id') + "Text";
      var answer = $(dialog).html();
      $.ajax({
        url: '/Feedback',
        data: answer,
        type: 'POST',
        dataType: 'json',
        error: connectionError,
        success: function(result) {

          feedback = result.feedback;
        
          if(result.correct === true){
            play('Boo')
          }else{
            play('Clap')
            feedback+= " The correct answer is: <br /><br />" + result.correctAnswer            
          }
          
          if(result.finished === false) {
            $("#next").addClass('invisible')
            $("#nextQuestion").removeClass('invisible')            
          }else{
            $("#nextQuestion").addClass('invisible')
            $("#feedbackMainMenu").addClass('invisible')            
            $("#next").removeClass('invisible')
          }
          
          console.log(feedback)
          $('.section').addClass('invisible')
          $('#feedbackText').html(feedback)
          $('#commentText').html(result.comment)
          $('#finalScore').html(result.score)
          $('#feedback').removeClass('invisible')
          $('#score').removeClass('invisible')                    
          $('#feedbackButtons').removeClass('invisible')
        }
      });      
    })
    
    $('#nextQuestion').click(function(){
      var data = {action: 'next'}
      game(data)
      play('Grunty');      
    });
    
    $('#next').click(function(){
        $.ajax({
        url: '/End',
        type: 'GET',
        dataType: 'json',
        error: connectionError,
        success: function(result) {
          
          $('.section').addClass('invisible')
          $('#finalScore').html(result.score)
          $('#final').removeClass('invisible')
          $('#score').removeClass('invisible')          
          $('#finalButtons').removeClass('invisible')  
          if(result.score > 0){
            play('GoodEnd')
          }else{
            play('BadEnd')
          }         

        }
      });
    });
    
    $("#takeAgain").click(function(){
      var data = {action: 'retry'}
      game(data);
      play('Grunty');      
    })

  //----------------------------------------------------------------------------
  
  function game(data){
    $.ajax({
      url: '/Game',
      type: 'POST',
      data: data,
      dataType: 'json',
      error: connectionError,
      success: function(result) {

        $('.section').addClass('invisible')
        $('#questionText').html('Question ' + result.actual_question + ".- " + result.question.description)
        var answers = [result.question.answer, result.question.option1, result.question.option2, result.question.option3]
        shuffle(answers)
        $('#answer1Text').html(answers[0])
        $('#answer2Text').html(answers[1])
        $('#answer3Text').html(answers[2])
        $('#answer4Text').html(answers[3])
        $('#finalScore').html(result.score)
        $('#quiz').removeClass('invisible')
        $('#score').removeClass('invisible')
        $('#quizButtons').removeClass('invisible')
          
      }
    });
  }

  //----------------------------------------------------------------------------  
  
  function errorMessage(message) {
    $('body').css('cursor', 'auto');
    $('.section').addClass('invisible');
    $('#error_message').html(message);
    $('#error').removeClass('invisible');
  }
  
  //----------------------------------------------------------------------------
  
  function connectionError() {
    errorMessage('Error: Cannot connect to the server. Your sister wants a word with you... NOW!');
  }
  
  //----------------------------------------------------------------------------  
  
  function shuffle(array) {
    var currentIndex = array.length
      , temporaryValue
      , randomIndex
      ;

    while (0 !== currentIndex) {

      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
  
  //----------------------------------------------------------------------------
  
  function play(id_track){
    var audio = document.getElementById(id_track);
    audio.currentTime = 0;
    audio.play();
  }
  
})
