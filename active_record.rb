require 'sqlite3'

DataBase = SQLite3::Database.new("questions.dat")

# ---------------------------------------------
#                ACTIVE RECORD
# ---------------------------------------------

class Question
  
  attr_reader :id
  attr_accessor :description, :answer, :option1, :option2, :option3
  
  def initialize(description='', answer='', option1='', option2='', option3='')
    @description = description
    @answer = answer
    @option1 = option1
    @option2 = option2
    @option3 = option3    
    @id = -1
  end
  
  def save
    if @id == -1
      id = DataBase.get_first_value('SELECT MAX(ID) FROM questions')
      if id
        @id = id + 1
      else
        @id = 1
      end          
    end
    DataBase.execute('INSERT OR REPLACE INTO questions VALUES (?, ?, ?, ?, ?, ?)',
                     [@id, @description, @answer, @option1, @option2, @option3])
  end
  
  def self.create(description, answer, option1, option2, option3)
    question = Question.new(description, answer, option1, option2, option3)
    question.save
    question
  end
  
  def remove
    if @id != -1
      DataBase.execute('DELETE FROM questions WHERE ID = ?', [@id])
      @id = -1
    end    
  end
  
  def self.remove_all
    DataBase.execute('DELETE FROM questions')
  end
  
  def self.read(id)
    row = DataBase.get_first_row('SELECT * FROM questions WHERE ID = ?', [id])
    if row
      question = Question.new(row[1], row[2], row[3], row[4], row[5])
      question.instance_variable_set(:@id, row[0])
      question
    else
      nil
    end
  end
  
  def self.read_all
    DataBase.execute('SELECT * FROM questions').map do |row|
      question = Question.new(row[1], row[2], row[3], row[4], row[5])
      question.instance_variable_set(:@id, row[0])
      question
    end    
  end
  
  def self.size
    DataBase.execute('SELECT COUNT(*) FROM questions')[0][0]
  end
  
  def inspect
    "Question(id: #{ @id }, descrption: '#{ @description }', answer: #{ @answer }, option1: #{ @option1 }, option2: #{ @option2 }, option3: #{ @option3 })"
  end
end

# ---------------------------------------------
#             PROCESSING A CSV
# ---------------------------------------------

Question.remove_all
file = File.new("questions.csv", "r")
counter = 1
while (line = file.gets)
    if(line != '')
      line.strip!
      params = line.split(/;/)
      Question.create(params[0], params[1], params[2], params[3], params[4])
    end
    counter+= 1
end

file.close

puts "QUESTIONS ON DATABASE = #{Question.size}"
