#!/usr/bin/env ruby

# DESIGN PATTERN QUIZ
#
# EDWIN ANTONIO GONZÁLEZ URZUA 1167603
# GUILLERMO JUÁREZ DURÁN  1168346
#

require 'sinatra'
require 'json'
require './active_record'


# ---------------------------------------------
#                  SINATRA
# ---------------------------------------------
enable :sessions

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end

get '/' do
  redirect '/Quiz'
end

get '/Quiz' do
  session[:actual_q] = nil
  session[:q_ids] = nil
  session[:questions] = nil
  session[:score] = 0
  erb :index
end

get '/Quit' do
  session[:actual_q] = nil
  session[:q_ids] = nil
  session[:questions] = nil
  session[:score] = 0
end

get '/End' do
  result = {'score' => session[:score]}
  result.to_json
end

post '/' do
  redirect '/'
end

post '/Game' do
  body = request.body.read.split("&").map{|x| x.split(/\w+=/)}.flatten.reject!{|c| c.empty?}
  if (body[0] == 'none')
    questions = body[1].to_i
    session[:questions] = questions
    size = Question.size
    session[:q_ids] = (1..size).to_a.shuffle.slice(0, questions)
    session[:score] = 0
  elsif (body[0] == 'retry')
    session[:actual_q] = 0
    session[:score] = 0
  end
  
  session[:actual_q] = (session[:actual_q] || 0) + 1
  record = Question.read(session[:q_ids][session[:actual_q] - 1])
  session[:record] = record
  question = {'description' => record.description, 'answer' => record.answer, 'option1' => record.option1, 'option2' => record.option2, 'option3' => record.option3}
  result = {'actual_question' => session[:actual_q], 'question' => question, 'score' => session[:score]}
  result.to_json
end

post '/Feedback' do
  answer = request.body.read
  correctAnswer = nil
  if(answer == session[:record].answer)
    session[:score]+=100
    feedback = 'Correct Answer!'
    comment = 'Hear the crowd, they boo and hiss, to see you fail they would not miss!'
    correct = true
  else
    feedback = 'Wrong Answer!'
    comment = 'Get more wrong, yes that\'s the way, the crowds are happier today!'
    correct = false
    correctAnswer = session[:record].answer
  end
  finished = (session[:actual_q] == session[:questions]) ? true : false
  result = {'feedback' => feedback, 'comment' => comment, 'score' => session[:score], 'finished' => finished, 'correct' => correct, 'correctAnswer' => correctAnswer}
  result.to_json
end

not_found do
  halt 404, erb(:notfound)
end
